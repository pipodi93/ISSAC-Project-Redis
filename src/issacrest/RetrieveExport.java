package issacrest;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lambdaworks.redis.GeoArgs;
import com.lambdaworks.redis.GeoArgs.Unit;
import com.lambdaworks.redis.GeoWithin;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;

@Path("/export")
public class RetrieveExport {
	
	private static final int SEARCH_RADIUS = 200;
	private static final Unit RADIUS_UNIT = Unit.km;
	private static final String GEOLIST_NAME = "list";
	
	@GET
	@Produces("application/json")
	public Response retrieveExports(@HeaderParam("payload") String payload) throws JSONException{
		JSONObject payloadObj = new JSONObject(payload);
		RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
		StatefulRedisConnection<String, String> connection = redisClient.connect();
		RedisCommands<String, String> syncCommands = connection.sync();
		List<GeoWithin<String>> neighbors = syncCommands.georadiusbymember(RetrieveExport.GEOLIST_NAME, payloadObj.getString("key"), 
				RetrieveExport.SEARCH_RADIUS, RetrieveExport.RADIUS_UNIT, new GeoArgs().withDistance().asc());
		JSONArray responseArray = new JSONArray();
		for (GeoWithin<String> geoWithin : neighbors) {
				JSONObject responseObj = new JSONObject();
				responseObj.put("id", geoWithin.getMember());
				responseObj.put("value", syncCommands.get(geoWithin.getMember()));
				responseObj.put("dist", geoWithin.getDistance());
				responseArray.put(responseObj);
		}
		connection.close();
		redisClient.shutdown();
		System.out.println("Sending neighbors to: " + payloadObj.getString("key") + " | Export array: " + responseArray.toString());
		return Response.status(200).entity(responseArray.toString()).build();
	}
	
	@POST
	@Consumes("application/json")
	public Response saveExport(@HeaderParam("payload") String payload) throws JSONException{
		JSONObject payloadObj = new JSONObject(payload);
		System.out.println("Data received from: " + payloadObj.getString("key") + ", located at LAT: "
				+ payloadObj.getDouble("lat") + " and LNG: " + payloadObj.getDouble("lng") + " with EXPORT: " + payloadObj.getString("value"));
		RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
		StatefulRedisConnection<String, String> connection = redisClient.connect();
		RedisCommands<String, String> syncCommands = connection.sync();
		syncCommands.set(payloadObj.getString("key"), payloadObj.getString("value"));
		syncCommands.geoadd(RetrieveExport.GEOLIST_NAME, payloadObj.getDouble("lng"), 
				payloadObj.getDouble("lat"), payloadObj.getString("key"));
		return Response.status(200).entity(true).build();
	}
	
	@POST
	@Path("register")
	@Consumes("application/json")
	public Response registerDevice(@HeaderParam("payload") String payload) throws JSONException{
		JSONObject payloadObj = new JSONObject(payload);
		RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
		StatefulRedisConnection<String, String> connection = redisClient.connect();
		RedisCommands<String, String> syncCommands = connection.sync();
		int lastID = Integer.parseInt(syncCommands.get("lastID"));
		int deviceID = lastID + 1;
		System.out.println("Last ID: " + lastID + "| New deviceID: " + deviceID);
		syncCommands.set("lastID", String.valueOf(deviceID));
		syncCommands.set(String.valueOf(deviceID), "");
		JSONObject responseObj = new JSONObject();
		responseObj.put("result", true);
		responseObj.put("id", "" + deviceID);
		syncCommands.geoadd(RetrieveExport.GEOLIST_NAME, payloadObj.getDouble("lng"), 
				payloadObj.getDouble("lat"), "" + deviceID);
		return Response.status(200).entity(responseObj.toString()).build();
	}
}

